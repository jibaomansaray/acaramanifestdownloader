import { DOMParser } from "https://deno.land/x/deno_dom/deno-dom-wasm.ts";
import { ensureDir } from "https://deno.land/std@0.164.0/fs/mod.ts"

const url = "https://rdf.australiancurriculum.edu.au";

try {
  const res = await fetch(url);
  const html = await res.text();
  const doc = new DOMParser().parseFromString(html, "text/html");
  const downloadFolder = './downloads';

  await ensureDir(downloadFolder);

  const listItems = doc?.getElementsByTagName("li");
  const manifests: Array<string> = [];
  listItems?.forEach((item) => {
    const href = `${url}${item
      .getElementsByTagName("a")[1]
      .getAttribute("href")}`;
    manifests.push(href);
  });

  manifests.forEach((manifest) =>
    writeManifest(manifest, `${downloadFolder}/${manifest.split("elements/")[1].split("/")[2]}`)
  );
} catch (error) {
  console.log(error);
}

async function writeManifest(url: string, fileName: string) {
  const res = await fetch(url);
  const json = await res.json();
  const existingFiles: string[] = [];

  for await (const dirEntry of Deno.readDir("./downloads")) {
    if (dirEntry.isFile) {
      existingFiles.push(dirEntry.name);
    }
  }

  await Deno.writeTextFile(fileName, JSON.stringify(json));
}
